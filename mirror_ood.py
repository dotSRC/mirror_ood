#!/usr/bin/env python3
import argparse
import os
import subprocess
import sys
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
import re

from datetime import datetime as dt
from datetime import timedelta as td

syncfileformat = "{}-dotsrc-lastsync"
# Sync status folder under mirrors folder
syncfolder = "syncstatus"

# This is the variables to fetch from each mirror config file
sourcevars = ["NAME", "LASTSYNCFILE", "WARNLEVEL", "REMOTE_HOST"]

regex_match_exit = [
    re.compile(r"Exit status: (\d+)"),
    re.compile(r">> Log: Run end; exiting (\d+)\.")
]
regex_date = re.compile(r"(\d\d\d\d)-(\d\d)-(\d\d)-(\d\d)(\d\d)")
# Not perfect, but does a okay job of cutting of port numbers protocols and usernames,
# if something does not match, we just leave it empty.
regex_host = re.compile(r"([0-9-a-zA-Z.]+[a-zA-Z])[:0-9/]*$")


# Will evaluate config file in bash
# This assumes config files are not malicius
def read_mirror_config(path: Path):
    res = {}

    # TODO is there a better way
    cmd = ["env", "-i", "bash", "-c", f"source {str(path)} && set"]
    proc = subprocess.Popen(cmd, stdout = subprocess.PIPE)
    for line in proc.stdout:
        (key, _, value) = line.decode().partition("=")
        if key not in sourcevars:
            continue

        # Remove trailing newline
        value = value.rstrip("\n")

        res[key] = value
    proc.communicate()

    return res


class Status(Enum):
    GOOD = 0
    PARTIAL = 1
    FAILED= 2


def read_log_status(mirror_logdir: Path):
    def read_log_file(path: Path):
        exit_status = None
        with path.open() as f:
            for line in f:
                pass
            # We now have the last line in `line`
            for pattern in regex_match_exit:
                m = pattern.match(line)
                if m is None:
                    continue
                exit_status = int(m.group(1))
                break

        if exit_status in [23, 24, 25]:
            return Status.PARTIAL
        if exit_status == 0:
            return Status.GOOD
        if exit_status == None:
            return None
        else:
            return Status.FAILED

    def date_from_path(path: Path):
        m = regex_date.search(str(path))
        if m is None:
            return None
        return dt(
            int(m.group(1)), int(m.group(2)), int(m.group(3)), # Year, month, date
            int(m.group(4)), int(m.group(5)) # Hour, minute
            )

    entries = sorted(mirror_logdir.iterdir(), reverse=True)

    # If the newest logfile is incomplete, then we walk back until we reach
    # one that is for the last_completed date.
    last = None
    i = 0
    for entry in entries:
        last_completed = date_from_path(entry)
        if last_completed == None:
            continue
        status = read_log_file(entry)
        if status == None:
            continue

        if i == 0:
            first_status = status
            last = last_completed

        if status is not Status.FAILED:
            break

        i += 1
    else:
        # We go here is status is never GOOD or PARTIAL
        return Status.FAILED, None, last

    # Here we assume that first_status and last_completed have been set
    return first_status, last_completed, last


def read_lastsync_file(path: Path):
    if not path.is_file():
        return None

    with open(path) as f:
        timestamp = f.readline()

    return dt.utcfromtimestamp(int(timestamp))


@dataclass
class Mirror:
    name: str
    config_file: str
    last: dt
    last_completed: dt
    status: Status
    source: str
    warntime: dt

    @classmethod
    def load_status(cls, args, path: Path):
        # Read config file
        cfg = read_mirror_config(path)
        name = cfg["NAME"]
        raw_source = cfg["REMOTE_HOST"]
        if not name:
            return None

        warntime = dt.now() - td(seconds=int(cfg["WARNLEVEL"]))

        # Read log file
        logdir = args.logdir / name
        if not logdir.is_dir():
            # Mirrors without any logs are probably inactive
            return None

        status, last_completed, last = read_log_status(logdir)
        if last_completed == None:
            last_completed = read_lastsync_file(Path(cfg["LASTSYNCFILE"]))
        if last == None:
            last = last_completed

        source = ""
        sm = regex_host.search(raw_source)
        if sm is not None:
            source = sm.group(1)
            if source != raw_source:
                print(f"cut source {raw_source} to {source}", file=sys.stderr)

        return cls(
            config_file=str(path),
            name=name,
            last=last,
            last_completed=last_completed,
            status=status,
            source=source,
            warntime=warntime,
            )

    def oneline(self):
        return f"{self.status.name} last={self.last} last_com={self.last_completed} warntime={self.warntime} source={self.source} file={self.config_file} {self.name}"

    def ood_tag(self):
        if self.last_completed < self.warntime:
            return "ood"
        else:
            return ""


def for_mirror(args, func):
    if args.fromfile:
        with args.fromfile.open() as f:
            for line in f:
                line = line.rstrip("\n")
                func(line)
    else:
        for entry in os.scandir(args.mirrors):
            if entry.is_file():
                func(entry.name)


# Args should be argparsed
def cmd_status(args):
    def do_for_mirror(entry):
        path = args.mirrors / entry
        mr = Mirror.load_status(args, path)
        if mr is not None:
            print(mr.oneline())

    if args.mirror == None:
        for_mirror(args, do_for_mirror)
    else:
        do_for_mirror(args.mirror)


# This is done in a function so that we only import jinja when we need it
def prepare_jinja():
    from jinja2 import Environment, BaseLoader, select_autoescape, TemplateNotFound

    # Can't find a jinja2 loader which just loads a single file from a absolute
    # path. Taken from https://jinja.palletsprojects.com/en/2.11.x/api/#jinja2.BaseLoader
    class FileLoader(BaseLoader):
        def get_source(self, environment, template):
            if not os.path.exists(template):
                raise TemplateNotFound(template)
            mtime = os.path.getmtime(template)
            with open(template, "r") as f:
                source = f.read()
            return source, template, lambda: mtime == getmtime(template)

    # Why is this so complicated
    return Environment(
            loader=FileLoader(),
            autoescape=select_autoescape(['html', 'xml'])
            )


def friendly_timedelta(dur: td):
    years = dur.days / 365
    if int(years) > 0:
        return f"{round(years, 1)} years"
    weeks = dur.days / 7
    if int(weeks) > 0:
        return f"{round(weeks, 1)} weeks"
    days = dur.seconds / (60 * 60 * 24) + dur.days
    if int(days) > 0:
        return f"{round(days, 1)} days"
    hours = dur.seconds / (60 * 60)
    if int(hours) > 0:
        return f"{round(hours, 1)} hours"
    minutes = dur.seconds / 60
    if int(minutes) > 0:
        return f"{round(minutes, 1)} minutes"
    return f"{dur.seconds} seconds"


def cmd_template(args):
    env = prepare_jinja()
    now = dt.now()

    def print_date(date):
        return friendly_timedelta(now - date)

    # Context to pass to render
    ctx = {"now": dt.utcnow().strftime("%Y-%m-%d %H:%M:%S"),
           "mirrors": [], "prdate": print_date}

    def do_for_mirror(entry):
        mr = Mirror.load_status(args, args.mirrors / entry)
        if mr and mr.last > (now - td(days=100)):
            ctx["mirrors"].append(mr)

    for_mirror(args, do_for_mirror)

    t = env.get_template(str(args.template))
    render = t.render(ctx)
    if args.out:
        with open(args.out, "w") as f:
            f.write(render)
    else:
        sys.stdout.write(render)


# Setup argument parsers
parser = argparse.ArgumentParser(description="administrate mirrors")
parser.add_argument("--mirrors", "-m", type=Path, default=Path("mirrors"), help="directory to fetch mirror status and config from")
parser.add_argument("--fromfile", "-f", type=Path, help="only run on mirrors in given file")
parser.add_argument("--logdir", "-l", type=Path, default=Path("log"), help="Where to load log from")
subparsers = parser.add_subparsers(dest="cmd")

# Create sub commands
sub_status = subparsers.add_parser("status", help="inspect mirror status")
sub_status.add_argument("mirror", nargs="?")

sub_tmpl = subparsers.add_parser("template", help="generate using a given template file")
sub_tmpl.add_argument("template", type=Path, help="template file to pull")
sub_tmpl.add_argument("--out", "-o", type=Path, help="output rendered template, defaults to stdout")

args = parser.parse_args()

if args.cmd == "status":
    cmd_status(args)
elif args.cmd == "template":
    cmd_template(args)
else:
    parser.print_help()
